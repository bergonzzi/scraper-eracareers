# -*- coding: utf-8 -*-

import scrapy
from scrapy.contrib.loader import ItemLoader
from scrapy.contrib.loader.processor import MapCompose
from w3lib.html import remove_tags


class EraItem(scrapy.Item):
    id = scrapy.Field()
    ref = scrapy.Field()
    job = scrapy.Field()
    main_field = scrapy.Field()
    sub_field = scrapy.Field()
    summary = scrapy.Field()
    vacancies = scrapy.Field()
    contract = scrapy.Field()
    country = scrapy.Field()
    city = scrapy.Field()
    institute = scrapy.Field()
    deadline = scrapy.Field()
    url = scrapy.Field()

    # Contact info
    org_name = scrapy.Field()
    email = scrapy.Field()
    website = scrapy.Field()


class EraLoader(ItemLoader):
    summary_out = MapCompose(remove_tags)