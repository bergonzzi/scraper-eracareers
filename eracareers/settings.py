# -*- coding: utf-8 -*-

from scrapy import log

BOT_NAME = 'eracareers'

SPIDER_MODULES = ['eracareers.spiders']
NEWSPIDER_MODULE = 'eracareers.spiders'

DOWNLOADER_MIDDLEWARES = {
    'scrapy.contrib.downloadermiddleware.httpcache.HttpCacheMiddleware': 50,
    'eracareers.middlewares.random_proxy.ProxyMiddleware': 100,
    'scrapy.contrib.downloadermiddleware.httpproxy.HttpProxyMiddleware': 110,
    'scrapy.contrib.downloadermiddleware.useragent.UserAgentMiddleware': None,
    'eracareers.middlewares.random_useragent.RandomUserAgentMiddleware': 400,
}

# Custom exporter to enforce field order
FEED_EXPORTERS = {
    'csv': 'eracareers.middlewares.csv_exporter.MyCsvItemExporter'
}

ITEM_PIPELINES = {
    'eracareers.scrapyelasticsearch.ElasticSearchPipeline': 100,
}

ELASTICSEARCH_SERVER = 'localhost'
ELASTICSEARCH_PORT = 9200
ELASTICSEARCH_USERNAME = ''
ELASTICSEARCH_PASSWORD = ''
ELASTICSEARCH_INDEX = 'eracareers'
ELASTICSEARCH_TYPE = 'items'
ELASTICSEARCH_UNIQ_KEY = 'id'
ELASTICSEARCH_LOG_LEVEL = log.DEBUG

FIELDS_TO_EXPORT = [
    'ref',
    'job',
    'summary',
    'main_field',
    'sub_field',
    'vacancies',
    'contract',
    'country',
    'city',
    'institute',
    'deadline',
    'org_name',
    'email',
    'website',
    'id',
    'url'
]

HTTPCACHE_ENABLED = True
HTTPCACHE_STORAGE = 'scrapy.contrib.httpcache.LeveldbCacheStorage'
PROXY_LIST = 'eracareers/middlewares/proxies.txt'